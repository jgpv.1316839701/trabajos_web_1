//VARIABLES
const nombre = document.getElementById("nombre")
const apellido = document.getElementById("apellido")
const email = document.getElementById("correo")
const pass = document.getElementById("contraseña")
const tlf = document.getElementById("telefono")
const ced = document.getElementById("cedula")
const form = document.getElementById("form")
const parrafo = document.getElementById("warnings")

form.addEventListener("submit", e=>{
    e.preventDefault()
    let warnings = ""
    let entrar = false
    let regexEmail = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/
    parrafo.innerHTML = ""    
    if(nombre.value.length < 1){
        warnings += `¡¡Escriba sus Nombres!!<br>`
        entrar = true
    }
    if(apellido.value.length < 1){
        warnings += `¡¡Escriba sus Apellidos!!<br>`
        entrar = true
    }
    if(!regexEmail.test(email.value)){
        warnings += `¡¡El email no es correcto!!<br>`
        entrar = true
    }
    if(pass.value.length < 6){
        warnings += `¡¡La contraseña es muy corta!!<br>`
        entrar = true
    }
    if(tlf.value.length < 10){
        
        warnings += `¡¡El telefono no esta correcto!!<br>`
        entrar = true
    }
    if(ced.value.length < 10){
        
        warnings += `¡¡La cedula no esta correcto!!<br>`
        entrar = true
    }

    if(entrar){
        parrafo.innerHTML = warnings
    }else{
        parrafo.innerHTML = "ENVIADO CON EXITO"
        alert("DATOS ENVIADOS CORRECTAMENTE");
    }
})