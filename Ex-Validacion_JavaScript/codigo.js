/*Crear una función JavaScript. Realizar la validación de datos de alquiler de vehículos en JavaScript: 
código=alfanumérico de 5 caracteres, 
marca=alfanumérico de 50 caracteres, 
modelo=alfanumérico de 30 caracteres, 
año=numérico de 4 dígitos, 
fecha inicial=tipo date, 
fecha final=tipo date). 
La fecha de final debe ser mayor a la fecha inicial.
*/


//VARIABLES
const  cd = document.getElementById("cd")
const marca = document.getElementById("marca")
const modelo = document.getElementById("modelo")
const ano = document.getElementById("ano")
const fi = document.getElementById("fi")
const ff = document.getElementById("ff")
const form = document.getElementById("form")

form.addEventListener("submit", e=>{
    e.preventDefault()
    let entrar = false
      
    if(cd.value.length <5){
        warnings += `¡¡El codigo tiene que ser de 5 caracteres!!<br>`
        entrar = true
    }
    if(marca.value.length <50){
        warnings += `¡¡El codigo tiene que ser de 5 caracteres!!<br>`
        entrar = true
    }
    if(modelo.value.length <30){
        warnings += `¡¡El codigo tiene que ser de 5 caracteres!!<br>`
        entrar = true
    }
    if(form.ano.value==0){
        alert(`Ingrese el año`);
        form.ano.value="";
        form.fefinal.focus();
        entrar = true
      
    }
    if(form.fi.value==0){
        alert(`Ingrese una fecha inicial`);
        form.fi.value="";
        form.fi.focus();
        entrar = true

    }
    if(form.ff.value==0){
        alert(`Ingrese una fecha final`);
        form.ff.value="";
        form.ff.focus();
        entrar = true
    }
    if (Date.parse(ff) < Date.parse(fi)) {
        alert(` Las fechas ingresadas no estan correctas`) ;
        form.ff.value="";
        form.ff.focus();
        entrar = true
    }
}
)